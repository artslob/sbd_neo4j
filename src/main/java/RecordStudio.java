import api.InputExecutor;
import api.StartUpExecutor;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;

public class RecordStudio implements AutoCloseable {

    private final Driver driver;

    public RecordStudio(String uri, String user, String password) {
        driver = GraphDatabase.driver(uri, AuthTokens.basic(user, password));
    }

    @Override
    public void close() throws Exception {
        driver.close();
    }

    public static void main(String... args) throws Exception {
        try (RecordStudio recordStudio = new RecordStudio("bolt://localhost:7687", "neo4j", "myneo4j")) {

            StartUpExecutor startUpExecutor = new StartUpExecutor();
            startUpExecutor.start(recordStudio.driver);

            InputExecutor inputExecutor = new InputExecutor();
            inputExecutor.start(recordStudio.driver);
        }
    }
}
