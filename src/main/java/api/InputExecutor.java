package api;

import api.entities.AbstractNode;
import api.entities.SongNode;
import api.entities.UserNode;
import org.neo4j.driver.v1.Driver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputExecutor {
    public void start(Driver driver) {
        help();
        System.out.print("Please, enter your commands:\n$");
        try (Scanner sc = new Scanner(System.in)) {
            while (sc.hasNext()) {
                String[] input = parseInputWithQuotes( sc.nextLine() );

                switch (input[0]) {
                    case "select":
                    case "update":
                    case "insert":
                    case "delete":
                        try {
                            execute(driver, input);
                        } catch (Exception e){
                            System.out.println("Some problems while executing: " + e);
                        }
                        break;
                    case "help":
                        help();
                        break;
                    case "exit":
                        return;
                    default:
                        System.out.println("Unknown command.");
                        break;
                }
                System.out.print("$");
            }
        }
    }

    private void execute(Driver driver, String[] params) {
        if (params.length < 2){
            System.out.println("Not enough arguments to execute command.");
            return;
        }
        final String command = params[0];
        final String table_name = params[1];
        final AbstractNode table = getNodeByName(table_name);
        if (table == null){
            System.out.println("Table " + table_name + " doesn't exist.");
            return;
        }
        switch (command.toLowerCase()) {
            case "insert":
                table.create(driver, Arrays.copyOfRange(params, 2, params.length));
                break;
            case "update":
                table.update(driver, Arrays.copyOfRange(params, 2, params.length));
                break;
            case "select":
                String output = table.read(driver, Arrays.copyOfRange(params, 2, params.length));
                System.out.println(output);
                break;
            case "delete":
                table.delete(driver, Arrays.copyOfRange(params, 2, params.length));
                break;
            default:
                System.out.println("Command not found: " + command);
                return;
        }
        System.out.println("Success!");
    }

    private AbstractNode getNodeByName(String nodeName){
        switch (nodeName.toLowerCase()){
            case "song":
                return new SongNode();
            case "user":
                return new UserNode();
            default:
                return null;
        }
    }

    private void help() {
        System.out.println("Input Executor.");
        System.out.println("Help:");
        System.out.println("\t[ (command table params) | help | exit ]");
        System.out.println("\tcommand: select, update, insert or delete");
    }

    private String[] parseInputWithQuotes(String input){
        String regex = "\"([^\"]*)\"|(\\S+)";
        List<String> params = new ArrayList<>();
        Matcher m = Pattern.compile(regex).matcher(input);
        while (m.find()) {
            if (m.group(1) != null) {
                params.add(m.group(1));
            } else {
                params.add(m.group(2));
            }
        }
        return params.toArray(new String[0]);
    }
}
