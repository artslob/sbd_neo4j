package api.entities;

import org.neo4j.driver.v1.*;

public class UserNode extends AbstractNode {
    @Override
    public void create(Driver driver, String... params) {
        //TODO: check params count
        try (Session session = driver.session()) {
            try (Transaction tr = session.beginTransaction() ) {
                tr.run("MATCH (au:Author {name: $author_name})-[h:HAS]->" +
                        "(al:Album {name: $album_name})-[c:CONTAINS]->(s:Song {name: $song_name}) " +
                        "MERGE (u:User {name: $user_name}) MERGE (u)-[:LIKES]->(s)",
                        Values.parameters("author_name", params[0],
                                "album_name", params[1], "song_name", params[2], "user_name", params[3]));
                tr.success();
            }
        }
    }

    @Override
    public String read(Driver driver, String... params) {
        try (Session session = driver.session()){
            return session.readTransaction(tr -> {
                StatementResult result = tr.run(
                        "MATCH (u:User {name: $user_name}) RETURN u.name LIMIT 1",
                        Values.parameters("user_name", params[0]));
                if (result.hasNext())
                    return result.next().get(0).toString();
                else return null;
            });
        }
    }

    @Override
    public void update(Driver driver, String... params) {
        try (Session session = driver.session()) {
            try (Transaction tr = session.beginTransaction() ) {
                tr.run("MATCH (u:User {name: $user_name}) SET u.name = $new_value",
                        Values.parameters("user_name", params[0], "new_value", params[1]));
                tr.success();
            }
        }
    }

    @Override
    public void delete(Driver driver, String... params) {
        try (Session session = driver.session()) {
            try (Transaction tr = session.beginTransaction() ) {
                tr.run("MATCH (u:User {name: $user_name}) DETACH DELETE u",
                        Values.parameters("user_name", params[0]));
                tr.success();
            }
        }
    }
}
