package api.entities;

import org.neo4j.driver.v1.Driver;

public abstract class AbstractNode {
    public abstract void create(final Driver driver, final String... params);
    public abstract String read(final Driver driver, final String... params);
    public abstract void update(final Driver driver, final String... params);
    public abstract void delete(final Driver driver, final String... params);
}
