package api.entities;

import org.neo4j.driver.v1.*;

public class SongNode extends AbstractNode {

    @Override
    public void create(Driver driver, String... params) {
        try (Session session = driver.session()) {
            try (Transaction tr = session.beginTransaction() ) {
                tr.run("MERGE (au:Author {name: $author_name}) " +
                        "MERGE (au)-[h:HAS]->(al:Album {name: $album_name}) " +
                        "MERGE (al)-[c:CONTAINS]->(s:Song {name: $song_name})",
                        Values.parameters("author_name", params[0], "album_name", params[1], "song_name", params[2]));
                tr.success();
            }
        }
    }

    @Override
    public String read(Driver driver, String... params) {
        try (Session session = driver.session()){
            return session.readTransaction(tr -> {
                StatementResult result = tr.run(
                        "MATCH (au:Author)-[:HAS]->(al:Album)-[:CONTAINS]->(s:Song {name: $song_name}) RETURN au.name + ' ' + al.name + ' ' + s.name",
                        Values.parameters("song_name", params[0]));
                if (result.hasNext())
                    return result.next().get(0).toString();
                else return null;
            });
        }
    }

    @Override
    public void update(Driver driver, String... params) {
        try (Session session = driver.session()) {
            try (Transaction tr = session.beginTransaction() ) {
                tr.run("MATCH (au:Author {name: $author_name})-[:HAS]->(al:Album {name: $album_name})-[:CONTAINS]->(s:Song {name: $song_name}) " +
                        "SET s.name = $new_value",
                        Values.parameters("author_name", params[0], "album_name", params[1], "song_name", params[2], "new_value", params[3]));
                tr.success();
            }
        }
    }

    @Override
    public void delete(Driver driver, String... params) {
        try (Session session = driver.session()) {
            try (Transaction tr = session.beginTransaction() ) {
                tr.run("MATCH (au:Author {name: $author_name})-[:HAS]->(al:Album {name: $album_name})-[:CONTAINS]->(s:Song {name: $song_name}) " +
                        "DETACH DELETE s",
                        Values.parameters("author_name", params[0], "album_name", params[1], "song_name", params[2]));
                tr.success();
            }
        }
    }
}
