package api;

import api.entities.SongNode;
import org.neo4j.driver.v1.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Scanner;

public class StartUpExecutor {
    private final String PATH = "fill_tables";

    public void start(Driver driver) {
        if (!isGraphEmpty(driver)) {
            System.out.println("Graph is not empty");
            return;
        }
        System.out.println("Graph is empty, now its would filled up automatically.");

        Instant start = Instant.now();
        create_songs(driver);
        create_users(driver);
        System.out.println("Filling table took " + Duration.between(Instant.now(), start));
    }

    private void create_users(Driver driver){
        final DateFormat hms = new SimpleDateFormat("HH:mm:ss");
        final int quantityOfUsersPerIteration = 5_000; //TODO: change it
        final int iterations = 4;

        for (int i = 1; i <= iterations; i++) {
            long start = System.currentTimeMillis();
            try (Session session = driver.session()) {
                //TODO:try write transaction
                try (Transaction tr = session.beginTransaction()) {
                    tr.run("match (s:Song) " +
                            "with collect(s) as song_list " +
                            "with  range($lower_range, $upper_range) as r, song_list " +
                            "foreach (el in r| " +
                            "   create (u:User{name:\"user\" + el}) " +
                            "   foreach (song in song_list| " +
                            "       create (u)-[:LIKES]->(song) " +
                            "   ) " +
                            ")",
                            Values.parameters("lower_range", (i - 1) * quantityOfUsersPerIteration,
                                              "upper_range", i * quantityOfUsersPerIteration - 1));
                    tr.success();
                }
            }
            long end = System.currentTimeMillis();
            System.out.println(i + ") " + hms.format(new Date()) +
                    " " + ((i - 1) * quantityOfUsersPerIteration) + "-" + (i * quantityOfUsersPerIteration - 1) +
                    " users inserted for " + ((end - start) / 1000.0f));
        }
    }

    private void create_songs(Driver driver){
        URL url = this.getClass().getClassLoader().getResource(PATH);
        File folder;
        try {
            folder = new File(url.toURI());
        } catch (URISyntaxException e) {
            folder = new File(url.getPath());
        } catch (NullPointerException e) {
            System.out.println("Bad PATH in fill_table.");
            return;
        }
        final File[] authors_folder = folder.listFiles();

        final SongNode songNode = new SongNode();

        for (File author_file : authors_folder) {
            final File[] albums_files = author_file.listFiles();
            for (File album_file : albums_files) {
                try (Scanner sc = new Scanner(album_file)) {
                    final String author = author_file.getName();
                    final String album = album_file.getName();
                    while (sc.hasNext()) {
                        final String song = sc.nextLine();
                        songNode.create(driver, author, album, song);
                    }
                } catch (FileNotFoundException e) {
                    System.out.println("Scanner for file cant be opened.");
                }
            }
        }
    }

    private boolean isGraphEmpty(Driver driver) {
        final int LIMIT_FOR_COUNT = 1000;
        try (Session session = driver.session()) {
            return session.readTransaction(tr -> {
                StatementResult result = tr.run("MATCH (n) RETURN count(n) LIMIT $limit",
                        Values.parameters("limit", LIMIT_FOR_COUNT));
                return !result.hasNext() || result.next().get(0).asInt() < LIMIT_FOR_COUNT;
            });
        }
    }
}
